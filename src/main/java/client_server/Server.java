package client_server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) throws IOException {
        try {
            System.out.println("Server ready!");
            try (ServerSocket server = new ServerSocket(4004);
                 Socket clientSocket = server.accept()){

                try (BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                     BufferedWriter out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()))) {

                    String word = in.readLine();
                    System.out.println(word);

                    out.write("Hello, this server! you written : " + word + "\n");
                    out.flush();
                }
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }

}
