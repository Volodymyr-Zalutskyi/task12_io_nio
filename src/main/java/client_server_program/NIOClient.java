package client_server_program;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;

public class NIOClient extends Message {
    public static void main(String[] args) throws IOException {
        InetSocketAddress address = new InetSocketAddress("localhost", 8090);
        SocketChannel channel = SocketChannel.open(address);

        log("Connecting to Server on port 8090...");

        ArrayList<String> companyDetails = new ArrayList<String>();
        companyDetails.add("Eleks");
        companyDetails.add("SoftServe");
        companyDetails.add("Intellias");
        companyDetails.add("Epam");

        for(String companyNames: companyDetails) {
            byte [] message = new String(companyNames).getBytes();
            ByteBuffer byteBuffer = ByteBuffer.wrap(message);
            channel.write(byteBuffer);

            log("sending: " + companyNames);
            byteBuffer.clear();

            // wait for 2 seconds before sending next message
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        channel.close();
        }

}
