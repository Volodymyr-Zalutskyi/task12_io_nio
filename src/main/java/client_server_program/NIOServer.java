package client_server_program;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;


public class NIOServer {
    public static void main(String[] args) throws IOException {

        // Selector: multiplexor of SelectableChannel objects
        // selector is open here
        Selector selector = Selector.open();

        // ServerSocketChannel: selectable channel for stream-oriented listening sockets
        ServerSocketChannel socket = ServerSocketChannel.open();
        InetSocketAddress address = new InetSocketAddress("localhost", 8090);

        // Binds the channel's socket to a local address and configures the socket to listen for connections
        socket.bind(address);

        // Adjusts this channel's blocking mode
        socket.configureBlocking(false);

        int ops = socket.validOps();

        SelectionKey selectionKey = socket.register(selector, ops);

        // Infinite loop..
        // Keep server running
        while(true) {
            Message.log("server is waiting for new connection and buffer select...");

            // Selects a set of keys whose corresponding channels are ready for I/O operations
            selector.select();

            // token representing the registration of a SelectableChannel with a Selector
            Set<SelectionKey> keys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = keys.iterator();

            while (iterator.hasNext()) {
                SelectionKey myKeys = iterator.next();

                // Tests whether this key's channel is ready to accept a new socket connection
                if(myKeys.isAcceptable()) {
                    SocketChannel client = socket.accept();

                    // Adjusts this channel's blocking mode to false
                    client.configureBlocking(false);

                    // Operation-set bit for read operations
                    client.register(selector, SelectionKey.OP_READ);
                    Message.log("Connection Accepted: " + client.getLocalAddress() + "\n");
                }
                // Tests whether this key's channel is ready for reading
                else if(myKeys.isReadable()) {
                    SocketChannel channel = (SocketChannel) myKeys.channel();
                    ByteBuffer byteBuffer = ByteBuffer.allocate(256);
                    channel.read(byteBuffer);
                    String result = new String(byteBuffer.array()).trim();

                    Message.log("Message received: " + result);

                    if(result.equals("Epam")) {
                        channel.close();
                        Message.log("\nIt's time to close connection as we got last company name 'Epam'");
                        Message.log("\nServer will keep running. Try running client again to establish new connection");
                    }
                }
                iterator.remove();
            }

        }

    }

}
