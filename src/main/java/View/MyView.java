package View;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    private Scanner input = new Scanner(System.in);

    public void printView() {
        menu = new LinkedHashMap<String, String>();
        menu.put("1", "1 - Serialize");
        menu.put("2", "2 - Compare performance reading, writing and buffered reader");
        menu.put("3", "3 - Reads a Java source-code file");
        menu.put("Q", "4 - Quit");

        methodMenu = new LinkedHashMap<String, Printable>();
        methodMenu.put("1", this::pressButton1);
        methodMenu.put("2", this::pressButton2);
        methodMenu.put("3", this::pressButton3);
        methodMenu.put("Q", this::pressButtonQ);
    }

    private void pressButton1() {
        System.out.println("1111");
    }

    private void pressButton2() {

    }

    private void pressButton3() {

    }
    private void pressButtonQ() {
        System.out.println("!)");
    }


    private void outputMenu() {
        System.out.println("\nMenu");
        for (String item : menu.values()) {
            System.out.println(item);
        }
    }

    public void showMenu() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        }
        while (!keyMenu.equals("Q"));
    }
}